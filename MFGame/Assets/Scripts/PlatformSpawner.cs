﻿using UnityEngine;
using System.Collections;

public class PlatformSpawner : MonoBehaviour {

    int numPlatforms = 0;
    public float spawnCooldown = 1;
    private float timeUntilSpawn = 4;
    public GameObject platformPrefab;

    // Use this for initialization
    void Start () {

        for (int i = 0; i < numPlatforms; i++)
        {
            SpawnPlatform();
        }
    }
	
	// Update is called once per frame
	void Update () {
        timeUntilSpawn -= Time.deltaTime;
        if (timeUntilSpawn <= 0)
        {
        
         timeUntilSpawn = spawnCooldown;
        }
    }

    public void SpawnPlatform()
    {
        Vector3 newPos = new Vector3(transform.position.x, transform.position.y, 0);
        GameObject octo = Instantiate(platformPrefab, newPos, Quaternion.identity) as GameObject;
    }
 
}
