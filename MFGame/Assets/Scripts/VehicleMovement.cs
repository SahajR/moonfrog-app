﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class VehicleMovement : MonoBehaviour {
    public float fuelAmount = 60f, drainSpeed = 2f;
    float distanceTravelled;
    Vector3 lastPosition, currentPostion;
    GameObject fuelSlider, bwheel, fwheel, chassis;
    public GameObject Car, Boat, Plane;
    Quaternion prevRotation;

	// Use this for initialization
	void Start () {
        lastPosition = transform.position;
        fuelSlider = GameObject.FindGameObjectWithTag("fuelIndicator");
        Debug.Log(fuelSlider);
        fuelSlider.GetComponent<Slider>().value = fuelAmount;
        bwheel = GameObject.FindGameObjectWithTag("bWheel");
        chassis = GameObject.FindGameObjectWithTag("chassis");
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.touchCount > 0 && fuelAmount > 0)
        {
            fuelAmount -= drainSpeed;
            bwheel.GetComponent<LPJointWheel>().MotorSpeed = -100f;
            fwheel.GetComponent<VehicleVelocity>().updateSpeed();
            fuelSlider.GetComponent<Slider>().value = fuelAmount;
        }

        if(currentPostion.y > lastPosition.y)
        {
            distanceTravelled += Vector3.Distance(lastPosition, currentPostion);
            lastPosition = transform.position;
        }

        Debug.Log(distanceTravelled);
    }

    public void changeToBoat()
    {
        currentPostion = transform.position;
        prevRotation = transform.rotation;
        //chassis.GetComponent<LPFixturePoly>().Density = 0.2f;
        GameObject.Instantiate(Boat, currentPostion, prevRotation);
        GameObject.Destroy(this);
        
    }

    public void changeToCar()
    {
        chassis.GetComponent<LPFixturePoly>().Density = 2f;
        GetComponent<Rigidbody2D>().AddForce(transform.forward * 200f);
    }
}
